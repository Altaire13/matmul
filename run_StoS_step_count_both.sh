#!/usr/bin/env bash

if [[ $# -lt 4 ]]
then
    echo Usage: $0 min_s max_s step count
    exit 0
fi

./run_StoS_step_count_mode.sh $1 $2 $3 $4 n
./run_StoS_step_count_mode.sh $1 $2 $3 $4 g


