#!/usr/bin/env python

import sys

def median(lst):
    lst = sorted(lst)
    n = len(lst)

    if n < 1:
        return None

    if n % 2 == 1:
        return lst[(n + 1) / 2 - 1]
    else:
        return float( lst[n / 2 - 1] + lst[n / 2] ) / 2.0

def main():
    if len(sys.argv) < 2:
        print 'Usage:', sys.argv[0], 'file'
        sys.exit(0)

    file = open(sys.argv[1])

    lines = file.read().splitlines()

    # (type, s1, s2, s3)     ->  times

    map = {}

    for line in lines:
        # (0)time (1)s1 (2)s2 (3)s3 (4)type
        spl = line.split()
        if len(line) == 0 or spl[0] == 'Time' or len(spl) < 5:
            continue

        #     (  type,          s1,          s2,          s3)
        tup = (spl[4], int(spl[1]), int(spl[2]), int(spl[3])) 

        cur_time = float(spl[0])

        if tup in map:
            map[tup] = map[tup] + [cur_time]
        else:    
            map[tup] = [cur_time]

    for i in sorted(map):
        # sizes, min, median, max, len
        print ' '.join([str(j) for j in i]), min(map[i]), median(map[i]), max(map[i]), len(map[i])
        #print map[i]


if __name__ == '__main__':
    main()

