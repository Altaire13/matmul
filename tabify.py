#!/usr/bin/env python

import sys

def main():
    if len(sys.argv) < 2:
        print 'Usage:', sys.argv[0], 'file'
        sys.exit(0)

    file = open(sys.argv[1])

    lines = file.read().splitlines()

    for line in lines:
        print ','.join(line.split())

if __name__ == '__main__':
    main()

