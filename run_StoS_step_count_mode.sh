#!/usr/bin/env bash

if [[ $# -lt 5 ]]
then
    echo Usage: $0 min_s max_s step count mode
    exit 0
fi



for (( i=$1; i<=$2; i += $3 ))
do
    Release/matmul $i $i $i $5 -r $4
done


