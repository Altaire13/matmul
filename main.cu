/*
 * main.cu
 *
 *  Created on: Mar 24, 2016
 *      Author: alexey
 */

#include <cstdio>
#include <string>
#include <cmath>

#include <stdexcept>

#include <cuda.h>

using std::stoul;

typedef float Type;

#define get_elem(array, Row, Column) \
    ((reinterpret_cast<Type*>(static_cast<char*>(array.ptr) + (Row) * array.pitch))[(Column)])

#define EXPECT_NO_CUDA_ERROR(err) \
    do { \
        (err) = cudaGetLastError(); \
        if ((err)) { \
            printf("Cuda error:\n"); \
            printf("\t%s: %s\n", cudaGetErrorName((err)), cudaGetErrorString((err))); \
        } \
    } while (0)

#define ASSERT_NO_CUDA_ERROR(err) \
    do { \
        EXPECT_NO_CUDA_ERROR(err); \
        if (err) { \
            return -1; \
        } \
    } while (0)

__global__ void matmul(cudaPitchedPtr mat_a, cudaPitchedPtr mat_b, cudaPitchedPtr mat_c) {
    uint idx = blockDim.x * blockIdx.x + threadIdx.x;
    uint idy = blockDim.y * blockIdx.y + threadIdx.y;

    if (idx >= mat_c.xsize || idy >= mat_c.ysize) {
        return;
    }

    get_elem(mat_c, idy, idx) = 0;

    for (size_t i = 0; i < mat_a.xsize; ++i) {
        get_elem(mat_c, idy, idx) += get_elem(mat_a, idy, i) * get_elem(mat_b, i, idx);
    }
}

enum class MatmulMode {
    Naive,
    GoodMem
};

const char* modeToStr(MatmulMode mode) {
    switch(mode) {
    case MatmulMode::Naive:
        return "naive";
    case MatmulMode::GoodMem:
        return "goodmem";
    default:
        return nullptr;
    }
}

void print_usage(const char* name) {
    printf("Usage: %s M N K [-c] [n | g] [-r repeat]\n", name);
    printf(" Multiply randomly generated matrices A[MxN] and B[NxK]\n");
    printf("  %-6s%s", "-c", "Check result on host\n");
}

int parse_args(int argc, char** argv, size_t& m, size_t& n, size_t& k, bool& check, MatmulMode& mode, size_t& repeat) {
    if (argc < 4) {
        print_usage(argv[0]);
        return -1;
    }

    m = stoul(argv[1]);
    n = stoul(argv[2]);
    k = stoul(argv[3]);

    argc -= 4;
    argv += 4;

    if (argc > 0) {
        switch(argv[0][0]) {
        case 'n':
            mode = MatmulMode::Naive;
            argc -= 1;
            argv += 1;
            break;
        case 'g':
            mode = MatmulMode::GoodMem;
            argc -= 1;
            argv += 1;
            break;
        default:
            mode = MatmulMode::GoodMem;
        }
    }

    if (argc > 0 && argv[0] == std::string("-c")) {
        check = true;
        argc -= 1;
        argv += 1;
    } else {
        check = false;
    }

    if (argc > 0 && argv[0] == std::string("-r")) {
        if (argc > 1) {
            repeat = std::stoul(argv[1]);
        } else {
            print_usage(argv[0]);
            return -1;
        }
    }

    return 0;
}

class MatmulBenchmark {
public:
    MatmulBenchmark(size_t m, size_t n, size_t k) :
            m(m), n(n), k(k), err(cudaSuccess), lastTime(0) {
        cudaEventCreate(&start);
        cudaEventCreate(&stop);

        mat_a = make_cudaPitchedPtr(nullptr, 0, n, m);
        mat_b = make_cudaPitchedPtr(nullptr, 0, k, n);
        mat_c = make_cudaPitchedPtr(nullptr, 0, k, m);

        mat_a_dev = make_cudaPitchedPtr(nullptr, 0, n, m);
        mat_b_dev = make_cudaPitchedPtr(nullptr, 0, k, n);
        mat_c_dev = make_cudaPitchedPtr(nullptr, 0, k, m);

        blockDim = dim3(32, 32);
        gridDim = dim3((m - 1) / 32 + 1, (k - 1) / 32 + 1);


        mat_a.ptr = malloc(m * n * sizeof(Type));
        mat_b.ptr = malloc(n * k * sizeof(Type));
        mat_c.ptr = malloc(m * k * sizeof(Type));

        if (mat_a.ptr == nullptr || mat_b.ptr == nullptr || mat_c.ptr == nullptr) {
            fprintf(stderr, "Failed host data allocation.\n");

            if (mat_a.ptr) free(mat_a.ptr);
            if (mat_b.ptr) free(mat_b.ptr);
            if (mat_c.ptr) free(mat_c.ptr);

            throw std::runtime_error("Failed host data allocation");
        }

        mat_a.pitch = n * sizeof(Type);
        mat_b.pitch = k * sizeof(Type);
        mat_c.pitch = k * sizeof(Type);
    }

    void randomizeInput() {
        for (size_t i = 0; i < m; ++i) {
            for (size_t j = 0; j < n; ++j) {
                get_elem(mat_a, i, j) = static_cast<Type>(rand()) / RAND_MAX;
            }
        }

        for (size_t i = 0; i < n; ++i) {
            for (size_t j = 0; j < k; ++j) {
                get_elem(mat_b, i, j) = static_cast<Type>(rand()) / RAND_MAX;
            }
        }
    }

    virtual int run(float* exec_time) = 0;

    int checkCudaMem() {
        if (mat_a_dev.ptr == nullptr || mat_b_dev.ptr == nullptr || mat_c_dev.ptr == nullptr) {
            fprintf(stderr, "Failed device data allocation.\n");

            if (mat_a_dev.ptr) cudaFree(mat_a_dev.ptr);
            if (mat_b_dev.ptr) cudaFree(mat_b_dev.ptr);
            if (mat_c_dev.ptr) cudaFree(mat_c_dev.ptr);

            return -1;
        }

        return 0;
    }

    int hostCheck(double* deviation) {
        cudaPitchedPtr mat_ch = {nullptr, k * sizeof(Type), k, m};
        mat_ch.ptr = malloc(m * k * sizeof(Type));

        if (mat_ch.ptr == nullptr) {
            fprintf(stderr, "Failed check matrix allocation.\n");

            return -1;
        }

        *deviation = 0.0;

        for (size_t i = 0; i < m; ++i) {
            for (size_t j = 0; j < k; ++j) {
                get_elem(mat_ch, i, j) = 0;

                for (size_t p = 0; p < n; ++p) {
                    get_elem(mat_ch, i, j) += get_elem(mat_a, i, p) * get_elem(mat_b, p, j);
                }

                *deviation += abs(get_elem(mat_ch, i, j) - get_elem(mat_c, i, j));
            }
        }

        free(mat_ch.ptr);

        return 0;
    }

    virtual ~MatmulBenchmark() {
        free(mat_a.ptr);
        free(mat_b.ptr);
        free(mat_c.ptr);
    }

protected:

    size_t m;
    size_t n;
    size_t k;

    cudaPitchedPtr mat_a;
    cudaPitchedPtr mat_b;
    cudaPitchedPtr mat_c;

    cudaPitchedPtr mat_a_dev;
    cudaPitchedPtr mat_b_dev;
    cudaPitchedPtr mat_c_dev;


    dim3 gridDim;
    dim3 blockDim;


    cudaError_t err;

    cudaEvent_t start, stop;

    float lastTime;
};

class NaiveBenchmark : public MatmulBenchmark {
public:
    NaiveBenchmark(size_t m, size_t n, size_t k) : MatmulBenchmark(m, n, k) {}

    int run(float* exec_time) {
        cudaMalloc(&mat_a_dev.ptr, m * n * sizeof(Type));
        cudaMalloc(&mat_b_dev.ptr, n * k * sizeof(Type));
        cudaMalloc(&mat_c_dev.ptr, m * k * sizeof(Type));

        if (checkCudaMem()) {
            return -1;
        }

        mat_a_dev.pitch = mat_a.xsize * sizeof(Type);
        mat_b_dev.pitch = mat_b.xsize * sizeof(Type);
        mat_c_dev.pitch = mat_c.xsize * sizeof(Type);

        cudaMemcpy(mat_a_dev.ptr, mat_a.ptr, m * n * sizeof(Type), cudaMemcpyHostToDevice);
        cudaMemcpy(mat_b_dev.ptr, mat_b.ptr, n * k * sizeof(Type), cudaMemcpyHostToDevice);


        cudaEventRecord(start, 0);

        matmul <<< gridDim, blockDim >>> (mat_a_dev, mat_b_dev, mat_c_dev);

        cudaEventRecord(stop, 0);

        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&lastTime, start, stop);

        cudaMemcpy(mat_a.ptr, mat_a_dev.ptr, m * k * sizeof(Type), cudaMemcpyDeviceToHost);

        cudaFree(mat_a_dev.ptr);
        cudaFree(mat_b_dev.ptr);
        cudaFree(mat_c_dev.ptr);

        ASSERT_NO_CUDA_ERROR(err);

        *exec_time = lastTime;

        return 0;
    }
};

class GoodmemBenchmark : public MatmulBenchmark {
public:
    GoodmemBenchmark(size_t m, size_t n, size_t k) : MatmulBenchmark(m, n, k) {}

    int run(float* exec_time) {

        cudaMallocPitch(&mat_a_dev.ptr, &mat_a_dev.pitch, n * sizeof(Type), m);
        cudaMallocPitch(&mat_b_dev.ptr, &mat_b_dev.pitch, k * sizeof(Type), n);
        cudaMallocPitch(&mat_c_dev.ptr, &mat_c_dev.pitch, k * sizeof(Type), m);

        if (checkCudaMem()) {
            return -1;
        }

        cudaMemcpy2D(mat_a_dev.ptr, mat_a_dev.pitch, mat_a.ptr, mat_a.pitch, mat_a.pitch, mat_a.ysize, cudaMemcpyHostToDevice);
        cudaMemcpy2D(mat_b_dev.ptr, mat_b_dev.pitch, mat_b.ptr, mat_b.pitch, mat_b.pitch, mat_b.ysize, cudaMemcpyHostToDevice);


        cudaEventRecord(start, 0);

        matmul <<< gridDim, blockDim >>> (mat_a_dev, mat_b_dev, mat_c_dev);

        cudaEventRecord(stop, 0);
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&lastTime, start, stop);

        cudaMemcpy2D(mat_c.ptr, mat_c.pitch, mat_c_dev.ptr, mat_c_dev.pitch, mat_c_dev.xsize * sizeof(Type), mat_c_dev.ysize, cudaMemcpyDeviceToHost);

        cudaFree(mat_a_dev.ptr);
        cudaFree(mat_b_dev.ptr);
        cudaFree(mat_c_dev.ptr);

        ASSERT_NO_CUDA_ERROR(err);

        *exec_time = lastTime;

        return 0;
    }
};

int main(int argc, char** argv) {
    size_t m, n, k;
    bool check;
    size_t repeat = 1;

    FILE* time_log = fopen("time.log", "a");

    MatmulMode mode;

    if (parse_args(argc, argv, m, n, k, check, mode, repeat)) {
        return 0;
    }

    MatmulBenchmark* bench;

    if (mode == MatmulMode::Naive) {
        bench = new NaiveBenchmark(m, n, k);
    } else if (mode == MatmulMode::GoodMem) {
        bench = new GoodmemBenchmark(m, n, k);
    } else {
        fprintf(stderr, "Wrong mode");
        return -1;
    }

    srand(0);

    bench->randomizeInput();

    float matmul_time;

    int multErr;

    for (size_t i = 0; i < repeat; ++i) {
        multErr = bench->run(&matmul_time);

        if (multErr) {
            fprintf(stderr, "Error during launch");
            return -1;
        }

        fprintf(stdout,   "%12f %5lu %5lu %5lu %s\n", matmul_time, m, n, k, modeToStr(mode));
        fprintf(time_log, "%12f %5lu %5lu %5lu %s\n", matmul_time, m, n, k, modeToStr(mode));
    }

    if (check) {
        double deviation = 0.0;

        bench->hostCheck(&deviation);

        printf("Deviation:\n\t%lf\n", deviation);
    }

    delete bench;

    return 0;
}

